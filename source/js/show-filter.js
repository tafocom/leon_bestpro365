'use strict';

const button = document.querySelector('.main__show-filter');
const filter = document.querySelector('.filter');

button.addEventListener('click', function(evt) {
  evt.preventDefault();
  filter.classList.toggle('filter--show');
});

