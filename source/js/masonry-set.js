'use strict';

const elems = Array.prototype.slice.call(document.querySelectorAll('.masonry'));

elems.forEach(elem => {
  let msnry = new Masonry(elem, {
    // options
    itemSelector: '.masonry__item',
    columnWidth: 250,
    gutter: 22,
    horizontalOrder: true
  });
});
