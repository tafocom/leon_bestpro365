'use strict';
// Аккордион
$('.toggle').click(function(e) {
  e.preventDefault();

  var $this = $(this);
  // Переключаем слайды
  if ($this.next().hasClass('show')) {
    $this.next().removeClass('show');
    $this.next().slideUp(350);

    $this.removeClass('toggle--active');
  } else {
    $this.parent().parent().find('li .inner').removeClass('show');
    $this.parent().parent().find('li .inner').slideUp(350);
    $this.next().toggleClass('show');
    $this.next().slideToggle(350);

    $this.parent().parent().find('li .toggle').removeClass('toggle--active');
    $this.addClass('toggle--active');
  }
  // Переключаем иконки
  if ($this.children('.minus').hasClass('hide-icon')) {
    $this.parent().parent().find('li .minus').addClass('hide-icon');
    $this.parent().parent().find('li .plus').removeClass('hide-icon');
    $this.children('.plus').addClass('hide-icon');
    $this.children('.minus').removeClass('hide-icon');
  } else {
    $this.children('.plus').removeClass('hide-icon');
    $this.children('.minus').addClass('hide-icon');
  }
});
