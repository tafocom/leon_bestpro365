'use strict';

const gulp = require('gulp');
const sass = require('gulp-sass');
const plumber = require('gulp-plumber');
const notify = require('gulp-notify');
const autoprefixer = require('gulp-autoprefixer');
const server = require('browser-sync').create();
// const pug = require('gulp-pug');
const minify = require('gulp-csso');
const rename = require('gulp-rename');
const svgSprite = require('gulp-svg-sprite');
const svgmin = require('gulp-svgmin');
const spritesmith = require('gulp.spritesmith');
const replace = require('gulp-replace');
const gulpIf = require('gulp-if');
const cheerio = require('gulp-cheerio');
const del = require('del');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const path = require('path');
const prettify = require('gulp-html-prettify');
const babel = require('gulp-babel');
const posthtml = require('gulp-posthtml');
const include = require('posthtml-include');
const mixin = require('posthtml-mixins');
const expressions = require('posthtml-expressions');

// Собирает sass в css, добавляет префиксы
gulp.task('style', () => {
  return gulp.src('source/sass/style.scss')
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'Styles',
        message: err.message
      }))
    }))
    .pipe(sass())
    .pipe(autoprefixer({
      cascade: false,
      grid: true,
      flexbox: true
    }))
    .pipe(gulp.dest('build/css'))
    .pipe(server.reload({ stream: true }));
});

// Собирает html
gulp.task('html', () => {
  return gulp.src('source/*.html')
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'HTML',
        message: err.message
      }))
    }))
    .pipe(posthtml([
      include(),
      mixin(),
      expressions()
    ]))
    .pipe(gulp.dest('build/'));
});

// Собирает pug в html
gulp.task('pug', () => {
  return gulp.src('source/*.pug')
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'Pug',
        message: err.message
      }))
    }))
    .pipe(pug())
    .pipe(prettify({ indent_char: '\t', indent_size: 1 }))
    .pipe(gulp.dest('build/'));
});

// Собирает js модули, в каждом модуле автоматически переписывает весь код в ES5
gulp.task('babel', () => {
  return gulp.src('source/js/*.js')
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'Babel',
        message: err.message
      }))
    }))
    .pipe(babel({
      presets: ['env']
    }))
    .pipe(gulp.dest('build/js'))
    .pipe(server.reload({ stream: true }));
});

// Запускает локальный сервер
gulp.task('serve', () => {
  server.init({
    server: './build',
    notify: false,
    open: true,
    ui: false
  });

  server.watch('build/*.html').on('change', server.reload);
});

// Очищает папку с production версией проекта
gulp.task('clean', () => {
  return del('build');
});

// Собирает SVG спрайт и минифицирует его
gulp.task('sprite:svg', () => {
  return gulp.src('source/icons/*.svg')
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'SVG sprite',
        message: err.message
      }))
    }))
    .pipe(svgmin({
      js2svg: {
        pretty: true
      }
    }))
    .pipe(cheerio({
      run: ($) => {
        $('[fill][fill!="none"]').removeAttr('fill');
        $('[style]').removeAttr('style');
      },
      parserOptions: { xmlMode: true }
    }))
    .pipe(replace('&gt;', '>'))
    .pipe(svgSprite({
      mode: {
        symbol: {
          sprite: '../sprite.svg'
        }
      }
    }))
    .pipe(gulp.dest('build/image'));
});

// Собирает PNG спрайт
gulp.task('sprite:png', () => {
  return gulp.src('source/icons/*.png')
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'PNG sprite',
        message: err.message
      }))
    }))
    .pipe(spritesmith({
      imgName: 'sprite.png',
      cssName: 'sprite.scss',
      algorithm: 'top-down',
      imgPath: '../image/sprite.png',
      padding: 20
    }))
    .pipe(gulpIf('*.scss', gulp.dest('sass/global'), gulp.dest('build/image')));
});

// Минифицирует css и js
gulp.task('minify', () => {
  return gulp.src(['build/css/style.css', 'build/js/*.js', '!build/js/*.min.js'])
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'minify',
        message: err.message
      }))
    }))
    .pipe(gulpIf('*css', minify(), uglify()))
    .pipe(rename((path) => {
      path.basename += '.min';
    }))
    .pipe(gulpIf('*css', gulp.dest('build/css'), gulp.dest('build/js')));
});

// Копирует шрифты, изображения, разметку, минифицированные стили и скрипты в папку с production версией проекта
gulp.task('copy', () => {
  return gulp.src([
    'source/fonts/**/*.{woff,woff2,ttf}',
    'source/image/**'
    // 'source/js/*.min.js',
    // 'source/css/style.min.css',
    // '*.html'
  ], {
    base: 'source'
  })
    .pipe(plumber({
      errorHandler: notify.onError(err => ({
        title: 'Copy',
        message: err.message
      }))
    }))
    .pipe(gulp.dest('build/'));
});

// Опримизирует изображения
gulp.task('images', () => {
  return gulp.src('build/image/**/*.{png,jpg,gif}')
    .pipe(imagemin([
      imagemin.optipng({ optimizationLevel: 3 }),
      imagemin.jpegtran({ progressive: true })
    ]))
    .pipe(gulp.dest('build/image'));
});

// Запускает отслеживание изменений scss, pug и js файлов
gulp.task('watch', () => {
  gulp.watch('source/blocks/**/*.{sass,scss}', gulp.series('style'));
  gulp.watch('source/sass/**/*.{sass,scss}', gulp.series('style'));
  // gulp.watch('source/**/*.pug', gulp.series('pug'));
  gulp.watch('source/js/**/*.js', gulp.series('babel'));
  gulp.watch('source/blocks/**/*.html', gulp.series('html'));
  gulp.watch('source/*.html', gulp.series('html'));
});

// Сборка development версии проекта
gulp.task('build', gulp.parallel('html', 'style', 'babel'));

// Запуск сервера, сборки development и отслеживание
gulp.task('start', gulp.series('build', gulp.parallel('serve', 'watch')));

// Сборка production версии проекта
gulp.task('build:production', gulp.series('clean', 'sprite:svg', 'sprite:png', 'build', 'minify', 'copy', 'images'));
