'use strict';

var elems = Array.prototype.slice.call(document.querySelectorAll('.masonry'));

elems.forEach(function (elem) {
  var msnry = new Masonry(elem, {
    // options
    itemSelector: '.masonry__item',
    columnWidth: 250,
    gutter: 22,
    horizontalOrder: true
  });
});