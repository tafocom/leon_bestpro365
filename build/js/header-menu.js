'use strict';

var header = document.querySelector('.header');
var hamburger = header.querySelector('.header__hamburger');
var headerList = header.querySelector('.header__list');
// const headerContent = header.querySelector('.header__content');

hamburger.addEventListener('click', function (evt) {
  // headerContent.classList.toggle('header__content--show');
  headerList.classList.toggle('header__list--show');
});