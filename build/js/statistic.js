'use strict';

var statistic = document.querySelector('.statistic');
var items = statistic.querySelectorAll('.statistic__item');

// НЕ работает в ie11
// items.forEach(function(item) {
//   const valueFirst = item.querySelector('.statistic__value--first');
//   const valueSecond = item.querySelector('.statistic__value--second');
//   const fillFirst = item.querySelector('.statistic__scale--first .statistic__fill');
//   const fillSecond = item.querySelector('.statistic__scale--second .statistic__fill');
//
//   if (parseInt(valueFirst.textContent) > parseInt(valueSecond.textContent)) {
//     fillFirst.classList.add('statistic__fill--green');
//     fillSecond.classList.add('statistic__fill--red');
//     valueFirst.classList.add('statistic__value--green');
//     valueSecond.classList.add('statistic__value--red');
//   } else {
//     fillFirst.classList.add('statistic__fill--red');
//     fillSecond.classList.add('statistic__fill--green');
//     valueFirst.classList.add('statistic__value--red');
//     valueSecond.classList.add('statistic__value--green');
//   }
// });

for (var i = 0; i < items.length; i++) {
  var valueFirst = items[i].querySelector('.statistic__value--first');
  var valueSecond = items[i].querySelector('.statistic__value--second');
  var fillFirst = items[i].querySelector('.statistic__scale--first .statistic__fill');
  var fillSecond = items[i].querySelector('.statistic__scale--second .statistic__fill');

  if (parseInt(valueFirst.textContent) > parseInt(valueSecond.textContent)) {
    fillFirst.classList.add('statistic__fill--green');
    fillSecond.classList.add('statistic__fill--red');
    valueFirst.classList.add('statistic__value--green');
    valueSecond.classList.add('statistic__value--red');
  } else {
    fillFirst.classList.add('statistic__fill--red');
    fillSecond.classList.add('statistic__fill--green');
    valueFirst.classList.add('statistic__value--red');
    valueSecond.classList.add('statistic__value--green');
  }
};