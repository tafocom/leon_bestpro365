'use strict';

var button = document.querySelector('.main__show-filter');
var filter = document.querySelector('.filter');

button.addEventListener('click', function (evt) {
  evt.preventDefault();
  filter.classList.toggle('filter--show');
});